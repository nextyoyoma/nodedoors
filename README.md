# nodeDoors README

Uses chrome puppeteer to interact with the ZKAccess web interface and open pre-selected doors.

## Table of Contents

- [Background](#background)
- [Prerequisites](#rerequisites)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)


## Background

The ZKAccess web application is an interface for interacting with the ZK Teco access control modules.
The application implements an interface to remotely open any door on the system.
However, the interface is not designed for regular use, requiring several clicks in order to open the door.
This application automates the interaction with the web app by 1)logging in as a privileged user to obtain a valid token and 2) using that token to hijack the app's own API to send commands.

Ideally, this process would not require user-style interaction with the web application; however, obtaining a cookie appears to require a "normal" login.
Additionally, Chrome puppeteer, which is built into the browser, provides management of the token and its expiration, as well as convenient methods for http operations.

## Prerequisites

Requires an existing ZK Access instance reachable from this application's host system. User must have appropriate credentials to access the "open doors" module.

## Installation

Download to your project directory on a server with nodeJS installed. Install with `npm install` from the project directory.

For production usage, use a process manager like pm2 to daemonize and manage the process.

## Configuration

Supply the credentials for the ZK Access instance in creds.js. For convenience, a template is provided (creds-template.js)

Currently, the selection of doors is hard-coded to the three most commonly used doors for the original use-case. To include additional doors, you will need to inspect the requests from the ZK Access application
to determine the 'id' of the door, then add additional containers in the web page. A future version could implement a templated construction that accepts door ids and names to dynamically
create containers for doors.

## Usage

Use the web interface to open doors. The only parameter that can be set by the user is the timeout until door re-lock.
