//app.js

var express = require('express');
var helmet = require('helmet');
var bodyParser = require('body-parser');
var app = express();
var config = require('config')

app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Root route

var root = require('./routes/webserver.js');
app.use('/',root);

//Add routes for Goshen Doors
var goshendoors = require('./routes/goshendoors.js');
app.use('/goshendoors',goshendoors);

//Static content
var statics = require('./routes/static.js')
app.use(statics)
app.use(express.static('node_modules/jquery-circle-progress/dist/'))

//Don't add middleware after this
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(error, req, res, next) {
  res.status(500).json({ message: error.message});
});


module.exports = app;
