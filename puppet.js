
const CREDS = require('./creds');


async function run(u,door,interval) {
  try {
    const puppeteer = require('puppeteer');//may need to go inside function?
    const openInterval = interval || 5; //default fallback
    const doorNum = door || 5; //default fallback
    const url = u || 'http://zkg.stfrancisschool.edu'; //default fallback

    const browser = await puppeteer.launch({
      userDataDir: "./userData/"
    });

    const page = await browser.newPage();

    const usernameField = '#id_username';
    const passwordField = '#id_password';
    const loginBtn = '#id_login';


    await page.goto(url);
    page.waitForNavigation();

    await page.click(usernameField);
    await page.type(CREDS.username);
    await page.click(passwordField);
    await page.type(CREDS.password);
    await page.click(loginBtn);

    await page.waitForSelector('#id_btn_option');

    stamp = getStamp();

    var baseUrl = url + '/iaccess/SendDoorData/?';

    //Unsure of the function of the timestamp, but it appears to be a required parameter. In the native web app, the timestamp sent corresponds to the time the command is sent
    var urlString = baseUrl + 'func=opendoor&type=part&data=' + doorNum + '&open_interval='+ openInterval +'&enable_no_tzs=false&stamp=' + stamp[0] + '&_=' + stamp[1];

    var response = await page.goto(urlString);

    var value = await response.json();
    browser.close()
    return value;

  } catch (err) {
    console.log(err);
    if (browser) browser.close();
    return err;
  }

  //run();

  function getStamp(){
    var time = new Date().getTime();
    var timePlus = time + 3;
    return [time.toString(), timePlus.toString()];
  }
}

exports.run = run;
