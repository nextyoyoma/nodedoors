//server.js
var app = require('./app.js');
var config = require('config');

var port = process.env.PORT || config.get('port');

var server = app.listen(port,function(){
  console.log('Express server listening on port ' + port);
});
