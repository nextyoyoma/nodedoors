
$(function(){
  //Add listener for links
  $('button.door-button').on('click',function(event) {
    event.preventDefault();
    var doorId = $(this).data('door')
    sendDoorData($(this).data('door'),$('input[data-door="' + doorId + '"]').val());
  });

  //Init indicator
  /*
  $('.indicator-container').radialIndicator({
  minValue: 0,
  maxValue: 5,
  initValue: 5,
  precision: 1,
  barColor: {
  0: '#FF0000',
  2: '#FFFF00',
  3: '#33CC33',
  10000: '#33CC33'
}
});
*/

$('.indicator').circleProgress({
  value: 0,
  size: 80,
  fill: {
    color: "green"
  }
})

$('.wait-container').hide()

});


function sendDoorData(doorNum,interval){
    var time = interval || 5
   $('.door-wrap[data-door="'+doorNum+'"]').find(".wait-container").show();

  $.get("goshendoors/" + doorNum + "/" + time + "/",(data,status,jqXHR) =>{
    $('.door-wrap[data-door="'+doorNum+'"]').find('.wait-container').hide();
    $('.door-wrap[data-door="'+doorNum+'"]').find('.indicator').circleProgress({animationStartValue:1, value: .0,animation: {duration: time * 1000}}); //Need to figure out how to find instance
  })
  .fail(function( jqXHR, textStatus, errorThrown ) {
    $('.door-wrap[data-door="'+doorNum+'"]').find('.wait-container').hide();
    alert(JSON.parse(jqXHR.responseText).message); //This is ugly. Must be a way to fix it.
  });
}
