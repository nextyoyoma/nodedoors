//webserver.js
var express = require('express');
var router = express.Router();
var path = require('path');

router.get('/', function(req,res,next){
  res.status(200).sendFile(path.resolve(__dirname + '/../html/index.html'));
});

module.exports = router;
