//goshendoors.js
var express = require('express');
var router = express.Router();

const url = "http://zkg.stfrancisschool.edu"; //refactor

var puppet = require('../puppet.js')

router.get("/:door/:interval",async function(req,res,next){
  //console.log("Got door: " + req.params.door)
  var door = req.params.door;
  var interval = req.params.interval;
  var val = await puppet.run(url,door,interval)
  if (val.result.length == 0 ) {
    console.log("Error. Result: " + val.result)
    setImmediate(() => { next(new Error('There was a problem opening the door.')); });
  } else {
    res.status(200).send(val);
    console.log(val);
  }
})


module.exports = router;
